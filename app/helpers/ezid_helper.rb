# frozen_string_literal: true

require "resque"
require "ezid-client"

# Helper methods for interacting with EZID (via the ezid-client library)
module EzidHelper
  # When a record is uploaded that has a location/target matching a
  # previously minted ARK, we assume it is a duplicate and instead of
  # minting a new ARK, we allow the user to update the metadata of the
  # existing ARK and sent the update to Ezid.
  #
  # This method is called by the Resque task.
  #
  # @param [ActionController::Parameters] updates
  # @param [Hash] options
  # @option options [Symbol] profile The metadata profile:
  #     :datacite, :dc, or :erc
  #
  # @return [Hash]
  def self.update_arks(options)
    success = true

    profile = options["profile"].to_s
    updates = options["updates"]
    user = options["user"]

    # We flag as duplicates any records with ARKs, whether or not
    # they're already in our database.  If they're not, we create an
    # entry for them below.  Set a job_id now, therefore, so we don't
    # get different times for each record.
    job_id = "#{user}_#{Time.now.to_i}"

    return [] if updates.blank?

    updates.each_with_index do |record, i|
      next if record["ark"].empty?

      Resque.logger.info "Updating #{i + 1}/#{updates.count}"

      begin
        Resque.logger.info "Querying for #{record["ark"]} ..."

        #
        # Update the record on EZID and in our database
        #
        identifier = Ezid::Identifier.find(record["ark"])

      # If we can't retrieve an ID there's no point trying to update
      # it; just log the error
      #
      # FIXME: this is also passing over auth errors that we should be
      # worried about
      rescue Ezid::Error => e
        Resque.logger.error "#{e} for object #{record.inspect}"
      rescue => e
        Resque.logger.error e
        success = false
        next
      end

      # Any uploaded record that has an ARK is treated as a duplicate,
      # even if it isn't in our system, so we create an entry for it
      # in the database if need be
      db_record = if record["id"].nil? || record["id"] == ""
                    {
                      created_by: [user],
                      job_id: job_id,
                      nonstandard_headers: {},
                      profile: profile,
                    }
                  else
                    Item.find_by(id: record["id"])
                  end

      record.each do |k, v|
        next if k == "id"

        if ApplicationHelper.standard_header? k
          Resque.logger.info "Setting #{k} => #{v}"

          db_record[k] = v
          if k == "target"
            identifier[:target] = v

          # Ezid gets very confused if you try to update a record with
          # a hash as value (which is what happens if we let
          # nonstandard_headers through)
          elsif ApplicationHelper.ezid_header? k
            identifier[ExcelHelper.normalize_header(k, profile)] = v
          end

        else
          Resque.logger.info "Adding #{k} => #{v} to nonstandard_headers"
          db_record[:nonstandard_headers][k] = v
        end
      end

      unless db_record[:created_by].include?(user)
        db_record[:created_by] << user
      end

      begin
        Resque.logger.info "Saved #{identifier.save}"
      rescue Ezid::Error => e
        Resque.logger.error "#{e} for object #{record.inspect}"
        success = false
        break
      end

      next if success == false

      begin
        if record["id"].nil? || record["id"] == ""
          Item.create(db_record)
        else
          db_record.save
        end
      rescue => e
        Resque.logger.error e
        success = false
      end
    end
    { success: success,
      job_id: job_id, }
  end

  # Mint the records provided and save them to the database.  This
  # method is run by Resque.
  #
  # @param [Hash] options Converted to JSON by Resque
  def self.mint(options)
    job_id = Time.now.to_i
    success = true

    profile = options["profile"].to_s
    user = options["user"]

    records = options["new"]
    total = records.count

    records.each_with_index do |obj, i|
      # The "Add row" feature means we might have empty rows even at
      # this late stage
      next if obj.all? { |_, v| v.empty? }

      # TODO: this might be dead code now
      obj.reject! do |k, v|
        # When we pull from the database, objects with columns (e.g.,
        # :target) un-set have that value set to nil, which confuses
        # Ezid.
        v.nil? ||
          #
          # We also don't want nils for :id and :ark; objects with
          # ARKs have already been send to minted, and these fields
          # will also confuse Ezid
          k.to_s == "ark" ||
          k.to_s == "id"
      end

      begin
        Resque.logger.info "Minting #{i + 1}/#{total}"
        ark = Ezid::Identifier.mint(
          # We don't want non-standard headers sent to Ezid
          obj.merge(profile: profile).select do |k, _|
            ApplicationHelper.standard_header?(k)
          end
        )
      rescue => e
        Resque.logger.error "#{e} for: #{obj.inspect}"
        success = false
        next
      end

      obj[:ark] = ark.to_s

      obj[:profile] = profile
      obj[:created_by] = [user]
      obj[:job_id] = "#{user}_#{job_id}"

      # Any non-standard headers (e.g., Aleph system numbers) need to
      # be preserved, but we can't just add columns to PSQL on the
      # fly.  So the solution is to have a column of non-standard
      # headers, the entries of which are JSON objects:
      #
      # {"System number":"2145842"}
      #
      # and so on.  The key is the header, and the value is that
      # record's value for that header.
      obj[:nonstandard_headers] = {}
      obj.reject { |k, _| ApplicationHelper.standard_header? k }.each do |k, v|
        obj[:nonstandard_headers][k.to_s] = v
      end

      # FIXME: https://github.library.ucsb.edu/ADRL/ARK-Minter/issues/11
      begin
        Item.create(obj.select { |k, _| ApplicationHelper.standard_header?(k) })
      rescue => e
        Resque.logger.error "PSQL Error: #{e} for: #{obj.inspect}"
        success = false
      end
    end
    { success: success,
      job_id: job_id, }
  end

  # Module for minting ARKs with Resque
  module Minter
    @queue = :minter

    # @param [ActionController::Parameters] job Rails parameters,
    # converted to JSON by Resque
    def self.perform(job)
      result = if job["updates"]
                 EzidHelper.update_arks(job)
               elsif job["new"]
                 EzidHelper.mint(job)
               end

      params = { email: job["email"],
                 job_id: result[:job_id], }

      if result && result[:success]
        MintedMailer.mint_complete(params).deliver_now
      else
        MintedMailer.mint_failed(params).deliver_now
      end
    end
  end
end
