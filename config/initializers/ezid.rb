# frozen_string_literal: true

require "ezid-client"

Ezid::Client.configure do |config|
  config.user = Rails.application.secrets.ezid_user
  config.password = Rails.application.secrets.ezid_pass
  config.default_shoulder = Rails.application.secrets.ezid_shoulder
end
