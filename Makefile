CONTROLLERS := $(wildcard app/controllers/*.rb)
HELPERS := $(wildcard app/helpers/*.rb)
MODELS := $(wildcard app/models/*.rb)

YARDOC := $(wildcard doc/*.html)

$(YARDOC): README.md $(CONTROLLERS) $(HELPERS) $(MODELS)
	bundle exec yardoc

all: doc/index.html

.PHONY: assets clean doc migrate rubocop server test vagrant worker

assets:
	RAILS_ENV=production bin/rake assets:precompile

clean:
	find . -name "*~" -exec rm {} \;

doc: all

migrate:
	bundle exec rake db:migrate

prod:
	ssh "adrl@$$SERVER" '/usr/local/bin/resque_stop.sh'
	REPO="$$REPO" bundle exec cap production deploy
	ssh -f "adrl@$$SERVER" '/usr/local/bin/resque_start.sh'

rubocop:
	rubocop --format simple --config .rubocop.yml --parallel

server:
	bundle exec rails server -b 0.0.0.0

test:
	RAILS_ENV=test bin/rake db:purge
	RAILS_ENV=test bin/rake db:migrate
	RAILS_ENV=test bin/rake db:fixtures:load
	bin/rake test --verbose

vagrant:
	ssh -p 2222 "deploy@127.0.0.1" '/usr/local/bin/resque_stop.sh'
	SERVER=127.0.0.1 REPO=/vagrant bundle exec cap vagrant deploy
	ssh -p 2222 -f "deploy@127.0.0.1" '/usr/local/bin/resque_start.sh'

worker:
	VERBOSE=1 INTERVAL=5 QUEUE=* bin/rake environment resque:work
