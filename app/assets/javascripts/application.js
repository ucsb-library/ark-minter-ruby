(function(){
  "use strict";
  //
  // Event listeners for the "Add row" buttons
  //
  var add_buttons = document.getElementsByClassName("add");
  var button = add_buttons.length;
  while (button--)
    add_buttons[button].addEventListener("click", addRow, false);

  function makeOptions(array) {
    var html = "",
        len = array.length;
    for (var n = 0; n < len; n++)
      html += "<option value='" + array[n] + "'>" + array[n] + "</option>";

    return html;
  }

  // Order matters; this is how they appear in the table
  var ELEMENTS = ["target", "ark", "id"];

  var RESOURCE_TYPES = [
    "Audiovisual",
    "Collection",
    "Dataset",
    "Event",
    "Image",
    "InteractiveResource",
    "Model",
    "PhysicalObject",
    "Service",
    "Software",
    "Sound",
    "Text",
    "Workflow",
    "Other"
  ];

  var table = document.getElementById('new');
  var headers = table.getElementsByTagName("tr")[0].children;

  var nonstandardHeaders = [];
  var index = headers.length;
  // Spaces aren't allowed in CSS classes, but we want to avoid
  // collisions if for some reason a user has columns "cat stuff" and
  // "catstuff"; therefore in EzidHelper.create_table we replace
  // spaces with an uncommon Unicode character.  Here we restore the
  // original string.
  while (index--) {
    if (!headers[index].classList.contains("standard_header"))
      nonstandardHeaders.push(headers[index].innerHTML.replace("〰", " "));
  }

  function addRow(e) {
    var classes = e.target.classList,
        rowContent = "",
        profile,
        elements,
        row = document.createElement("tr");

    if (classes.contains("erc")) {
      profile = "erc";
      // Order matters; this is how they appear in the table
      elements = ["erc_what", "erc_who", "erc_when"]
        .concat(ELEMENTS).concat(nonstandardHeaders);

    } else if (classes.contains("dc")) {
      profile = "dc";
      elements = ["dc_title",
                  "dc_creator",
                  "dc_type",
                  "dc_date",
                  "dc_publisher"].concat(ELEMENTS).concat(nonstandardHeaders);

    } else if (classes.contains("datacite")) {
      profile = "datacite";
      elements = ["datacite_title",
                  "datacite_creator",
                  "datacite_resourcetype",
                  "datacite_publicationyear",
                  "datacite_publisher"]
        .concat(ELEMENTS).concat(nonstandardHeaders);
    }

    var n = 0;
    // empty cell for displaying the row number
    rowContent += "<td class='line_number'></td>";
    for (n; n < elements.length; n++) {
      if (elements[n] === "datacite_resourcetype") {
        rowContent += "<td class='datacite_resourcetype'>" +
          "<select name='new[datacite][][datacite_resourcetype]'>";
        rowContent += makeOptions(RESOURCE_TYPES);
        rowContent += "</select></td>";
      }

      // If we're generating a new DataCite row, the Resource Type
      // is two parts: the general type, which is the <select>
      // element above, and the specific type, which can be
      // anything.  Here we change the name of the input so it
      // doesn't overwrite the parameter set by <select>.  We merge
      // the two values later.
      var profileElement = elements[n] === "datacite_resourcetype" ?
          "datacite_resourcesubtype" : elements[n];

      rowContent += "<td>";
      // Don't create an input element for ARK or database ID
      if (profileElement !== "ark" && profileElement !== "id") {

        rowContent += "<input class='" + profileElement +
          "' type='text' name='new[" + profile + ']';

        rowContent += "[][" + profileElement + "]'";

        if (profileElement !== "target")
          rowContent += " placeholder='Give me a value!'";

        rowContent += ">";

      } else if (profileElement === "ark") {
        rowContent += "<span class='ark'></span>";
      }
      rowContent += "</td>";
    }

    // Create a "Delete row" button for this row
    var delete_b = document.createElement("button");
    delete_b.setAttribute("class", "delete");
    delete_b.setAttribute("type", "button");
    delete_b.setAttribute("name", "button");
    delete_b.innerHTML = "✘";

    delete_b.addEventListener('click', deleteRow, false);

    var buttonCell = document.createElement("td");
    buttonCell.insertBefore(delete_b, null);

    table.insertBefore(row, null);
    row.innerHTML = rowContent;
    row.insertBefore(buttonCell, null);
  }

  //
  //
  // Event listeners for the "Delete row" buttons
  //
  var delete_buttons = document.getElementsByClassName("delete");
  button = delete_buttons.length;
  while (button--)
    delete_buttons[button].addEventListener('click', deleteRow, false);

  function deleteRow(e) {
    var row = e.target.parentNode.parentNode;
    var tbody = row.parentNode;

    // Don't delete the only row in a table
    if (tbody.children.length > 1)
      tbody.removeChild(row);
  }
})();
