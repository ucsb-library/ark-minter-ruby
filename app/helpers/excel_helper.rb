# coding: utf-8
# frozen_string_literal: true

require "pathname"
require "rubyXL"
require "spreadsheet"

# Helper methods for manipulating Excel files
module ExcelHelper
  # @param [ActionDispatch::Http::UploadedFile] file
  # @return [Array, RubyXL::Workbook]
  def self.open(file)
    ctype = file.content_type
    if ctype == "application/vnd.ms-excel"
      Spreadsheet.open(file.tempfile).worksheets
    elsif ctype == "application/"\
                   "vnd.openxmlformats-officedocument"\
                   ".spreadsheetml.sheet"
      RubyXL::Parser.parse(file.tempfile)
    else
      raise "Unknown content-type #{ctype}"
    end
  end

  # Extract Ezid-ready metadata from a spreadsheet.
  #
  # @param [Array, RubyXL::Workbook] book
  # @param [Hash] options
  #
  # @option options [Symbol] :profile Metadata profile: :datacite, :dc, or :erc
  #
  # @return [Hash]
  def self.scrape(book, options = {})
    profile = options.fetch(:profile).to_sym

    objects = {}
    objects[:updates] = {}
    objects[:new] = {}

    book.each do |sheet|
      # skip empty worksheets
      next if sheet.count == 0

      hashes = profile_hashes(sheet, profile)

      duplicates = ApplicationHelper.duplicates(
        hashes,
        Item.where.not(target: "")
      )

      hashes.each_with_index do |obj, i|
        if duplicates[i].nil?
          objects[:new][profile] ||= []
          objects[:new][profile] << obj
        else
          objects[:updates][profile] ||= []
          objects[:updates][profile] << duplicates[i]
        end
      end
    end

    # Remove duplicate duplicates 😸
    objects[:updates].keys.each do |p|
      objects[:updates][p].uniq!
    end

    objects
  end

  # Convert a header CELL to a standard format according to the
  # given PROFILE.
  #
  # @example
  #   normalize_header("What", :erc) #=> "erc.what"
  # @example
  #   normalize_header("Resource Type", :datacite) #=> "datacite.resourcetype"
  #
  # @param [String] cell the content of the header cell
  # @param [Symbol] profile the metadata profile (e.g., :erc, :dc)
  #
  # @return [String] the normalized header
  def self.normalize_header(cell, profile)
    profile.to_s +
      "." +
      cell.strip.tr(" ", "").downcase.sub(/^#{profile.to_s}[\._]/, "")
  end

  # Create an array of hashes that can be passed to
  # Ezid::Identifier.create
  #
  # @example
  #   profile_hashes(RubyXL::Worksheet, :erc)
  #   #=> [{:erc_what => "a cat", :erc_when => "2015"},
  #        {:erc_what => "computer", :target => "http://dell.com"}]
  #
  # @param [Spreadsheet::Excel::Worksheet, RubyXL::Worksheet] sheet
  # @param [Symbol] profile the metadata profile (:erc, :dc, :datacite)
  #
  # @return [Array<Hash>]
  def self.profile_hashes(sheet, profile)
    sheet_hashes = []
    profile_elements = if profile == :erc
                         ERC_ELEMENTS
                       elsif profile == :dc
                         DC_ELEMENTS
                       else
                         DATACITE_ELEMENTS
                       end

    # Remove blank rows from the top
    #
    # We can't remove all blank rows because deleting rows while
    # iterating over them is risky:
    # https://github.com/weshatheleopard/rubyXL/issues/207#issuecomment-153163829
    #
    # Empty rows in the middle of sheets are removed in new.html.erb
    # @see create_table
    sheet.delete_row(0) while sheet.first.nil?

    sheet.each_with_index do |row, i|
      # Skip the column headers
      next if i == 0

      # Skip empty rows
      next if row.nil?

      row = row.cells if row.respond_to?(:cells)

      # RubyXL rows don't respond to :each_with_index, so we have to
      # keep track of the index manually
      count = 0
      temp_elements = {}

      row.each do |cell|
        # Skip columns with empty headers
        if sheet.first[count].nil?
          count += 1
          next
        end

        # RubyXL cells have a .value, Spreadsheet cells don't.
        #
        # We're not checking cell.respond_to? because empty cells in
        # both RubyXL and Spreadsheet are nil, so this only works
        # for headers where we can expect them to be non-nil.
        if sheet.first[count].respond_to?(:value)
          header = sheet.first[count].value.strip
          cell = if cell.nil?
                   ""
                 else
                   cell.value
                 end
        else
          header = sheet.first[count].strip
          cell = "" if cell.nil?
        end

        # Convert floats to integers so years don't come out as "2015.0"
        cell = cell.to_i if /^[0-9\.]+$/ =~ cell.to_s

        # RubyXL adds this nonsense to date fields
        # https://help.library.ucsb.edu/browse/DEV-1063?focusedCommentId=64484&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-64484
        cell = cell.to_s.sub("T00:00:00+00:00", "")

        # Here is where we parse the column headers and match them to
        # metadata elements
        if header.downcase.start_with?("location", "target")
          temp_elements[:target] = cell
        elsif header.casecmp("ark").zero?
          temp_elements[:ark] = cell
        # If it's a standard header from one of the metadata
        # profiles (with an optional profile prefix; e.g.,
        # 'erc.what')
        elsif profile_elements.include?(header
                                          .downcase
                                          .sub(/^#{profile.to_s}[\._]/, ""))

          symb = normalize_header(
            header.downcase, profile
          ).sub(".", "_").to_sym

          temp_elements[symb] = cell
        elsif profile == :datacite && header =~ /resource\ ?type/i
          temp_elements[:datacite_resourcetype] = cell
        # If it's a non-standard header
        else
          temp_elements[header.to_sym] = cell
        end

        # Manually increment the index (RubyXL rows don't respond to
        # :each_with_index)
        count += 1
      end
      sheet_hashes << temp_elements
    end
    sheet_hashes
  end

  # Create and write to a new worksheet of an existing workbook.
  #
  # @param [RubyXL::Workbook] book
  # @param [Array] headers the headers to use for the worksheet
  # @param [Array] data an array of hashes, the values of which are
  #   the data for the worksheet
  #
  # @return [void]
  def self.write_sheet(book, headers, data)
    sheet = book.first

    headers.each_with_index do |header, i|
      sheet.add_cell(0, i, header)
    end

    data.each_with_index do |obj, i|
      i += 1
      count = 0
      obj.each_value do |v|
        sheet.add_cell(i, count, v)
        count += 1
      end
    end
  end
end
