# frozen_string_literal: true

class MintedMailer < ActionMailer::Base
  # @param [Hash] options
  # @option options [String] :email
  # @option options [String] :job_id
  def mint_complete(options)
    @job = options.fetch(:job_id)
    mail(
      to: options.fetch(:email), from: options.fetch(:email),
      subject: "Your ARKs have been minted! (job ID #{@job})"
    )
  end

  # @param [Hash] options
  # @option options [String] :email
  # @option options [String] :job_id
  def mint_failed(options)
    @job = options.fetch(:job_id)
    mail(
      to: options.fetch(:email), from: options.fetch(:email),
      subject: "Your ARKs were not minted (job ID #{@job})"
    )
  end
end
