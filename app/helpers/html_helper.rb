# coding: utf-8
# frozen_string_literal: true

#
# Methods for generating HTML for views
#
module HtmlHelper
  include ApplicationHelper

  # Sets the class of the <html> element, for styling purposes
  #
  # @param [ActionController::Parameters] params
  # @return [String]
  def htmlclass(params)
    hash = params.as_json
    return "simple" if hash["controller"] == "sessions"
    return "table" if hash["action"] == "archive"

    hash.delete "controller"
    hash.delete "action"
    return "simple" if hash.empty?
    "table"
  end
end
