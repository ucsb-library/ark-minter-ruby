# frozen_string_literal: true

require_relative "boot"
require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ArkMinter; end

class ArkMinter::Application < Rails::Application
  config.action_mailer.smtp_settings = {
    address: "sendmail-relay-2476.library.ucsb.edu",
    port: 25,
    enable_starttls_auto: true,
  }
end
