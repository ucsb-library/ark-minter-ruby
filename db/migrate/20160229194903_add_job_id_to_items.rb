class AddJobIdToItems < ActiveRecord::Migration[4.2]
  def change
    add_column :items, :job_id, :string
  end
end
