class AlterColumnItemsCreatedBy < ActiveRecord::Migration[4.2]
  def change
    change_column :items, :created_by, "text[] USING created_by::text[]"
  end
end
