# frozen_string_literal: true

PROFILE_SYMBOLS = [:datacite, :dc, :erc].freeze

ERC_ELEMENTS = %w[what who when].freeze
DC_ELEMENTS = %w[title creator type date publisher].freeze
DATACITE_ELEMENTS = %w[
  title
  creator
  resourcetype
  publicationyear
  publisher
].freeze

CONTROLLED_HEADERS =
  DATACITE_ELEMENTS +
  DC_ELEMENTS +
  ERC_ELEMENTS +
  %w[
    ark
    created_at
    created_by
    id
    job_id
    nonstandard_headers
    profile
    resource
    type
    target
    updated_at
  ]

EZID_HEADERS =
  CONTROLLED_HEADERS - %w[
    created_by
    created_at
    id
    job_id
    nonstandard_headers
    updated_at
  ]

# Controlled vocabulary for DataCite resource types:
# http://ezid.cdlib.org/doc/apidoc.html#profile-datacite
RESOURCE_TYPES_GENERAL = %w[
  Audiovisual
  Collection
  Dataset
  Event
  Image
  InteractiveResource
  Model
  PhysicalObject
  Service
  Software
  Sound
  Text
  Workflow
  Other
].freeze
