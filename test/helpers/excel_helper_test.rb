# frozen_string_literal: true

require "test_helper"

class ExcelHelperTest < ActiveSupport::TestCase
  test "find one duplicate (target) and one new record" do
    upload = Rack::Test::UploadedFile.new(
      Rails.root.join("test", "fixtures", "erc_0.xls"),
      "application/vnd.ms-excel",
      false
    )

    book = ExcelHelper.open(upload)
    records = ExcelHelper.scrape(
      book,
      profile: :erc
    )

    assert_equal 1, records[:updates][:erc].count
    assert_equal 1, records[:new][:erc].count
  end

  test "find two duplicates (ARKs)" do
    upload = Rack::Test::UploadedFile.new(
      Rails.root.join("test", "fixtures", "erc_minted.xlsx"),
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      false
    )

    book = ExcelHelper.open(upload)
    records = ExcelHelper.scrape(
      book,
      profile: :erc
    )

    assert_equal 2, records[:updates][:erc].count
    assert records[:new].empty?
  end

  test "find one new record" do
    upload = Rack::Test::UploadedFile.new(
      Rails.root.join("test", "fixtures", "erc_2.xlsx"),
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      false
    )

    book = ExcelHelper.open(upload)
    records = ExcelHelper.scrape(
      book,
      profile: :erc
    )

    assert_equal 1, records[:new][:erc].count
    assert records[:updates].empty?, "Found #{records[:dups].inspect}"
  end
end
