class AddDatacite < ActiveRecord::Migration[4.2]
  def change
    add_column :items, :datacite_creator, :string
    add_column :items, :datacite_title, :string
    add_column :items, :datacite_resourcetype, :string
    add_column :items, :datacite_publisher, :string
    add_column :items, :datacite_publicationyear, :string
  end
end
