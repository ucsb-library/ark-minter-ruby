class AddNonstandardHeadersToItems < ActiveRecord::Migration[4.2]
  def change
    add_column :items, :nonstandard_headers, :json
  end
end
