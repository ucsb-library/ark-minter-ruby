class CreateItems < ActiveRecord::Migration[4.2]
  def change
    create_table :items do |t|
      t.string :ark
      t.string :profile
      t.string :erc_who
      t.string :erc_what
      t.string :erc_when
      t.string :dc_creator
      t.string :dc_title
      t.string :dc_type
      t.string :dc_publisher
      t.string :dc_date
      t.string :target

      t.timestamps :null => false
    end
  end
end
