class AddCreatedBy < ActiveRecord::Migration[4.2]
  def change
    add_column :items, :created_by, :string
  end
end
