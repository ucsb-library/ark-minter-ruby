# frozen_string_literal: true

set :stage, :production
set :rails_env, "production"
set :default_env,
    "http_proxy" => "http://10.3.100.201:3128",
    "https_proxy" => "http://10.3.100.201:3128"

server ENV["SERVER"], user: "adrl", roles: [:web, :app, :db]
set :ssh_options, port: 22, keys: ["~/.ssh/id_rsa"]
