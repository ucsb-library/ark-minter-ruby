# frozen_string_literal: true

set :application, "ark-minter"
set :repo_url, ENV["REPO"]
set :deploy_to, "/var/www/ark-minter"

set :stages, %w[production vagrant]
set :default_stage, "vagrant"

set :log_level, :debug
set :bundle_flags, "--deployment"
set :bundle_env_variables, nokogiri_use_system_libraries: 1
set :passenger_restart_with_touch, true

set :keep_releases, 5
set :assets_prefix, "#{shared_path}/public/assets"

SSHKit.config.command_map[:rake] = "bundle exec rake"

set :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

set :linked_dirs, %w[tmp/pids tmp/cache tmp/sockets public/assets]
set :linked_files, %w[
  config/database.yml
  config/environments/production.rb
  config/environments/test.rb
  config/ezid.yml
  config/secrets.yml
]
