# coding: utf-8
# frozen_string_literal: true

require "net-ldap"

class SessionsController < ApplicationController
  def new
    @title = "Log in"

    return redirect_to "/new" if logged_in?
    return unless safari?

    flash[:warning] = "Safari performs very poorly "\
                      "with more than a few hundred records.  "\
                      "Consider using a different browser for large batches."
  end

  def create
    # The input field is `required`, but Safari ignores that so this
    # is necessary
    if params[:session][:password].empty?
      flash.now[:error] = "Please enter a password."
      return render "new"
    end

    # Active Directory always uses @library.ucsb.edu
    email = params[:session][:user].sub(/@(library\.)?ucsb\.edu/, "") +
            "@library.ucsb.edu"

    passw = params[:session][:password]

    ldap = Net::LDAP.new(
      host: "dc.library.ucsb.edu",
      auth: {
        method: :simple,
        username: email,
        password: passw,
      }
    )

    if ldap.bind
      connection = ldap.bind_as(
        base: "dc=library,dc=ucsb,dc=edu",
        filter: "(userprincipalname=#{email})",
        password: passw
      )

      if connection
        required_groups = [
          "OU=EzID",
          "OU=Global Security Groups",
          "CN=EzID-Editors",
        ]
        authed = required_groups.all? do |req|
          # the connection itself is an array, and every parameter is an array
          connection.first.memberof.any? do |group|
            group.downcase.include?(req.downcase)
          end
        end
      end

    # If the initial #bind didn't work, it's probably because of bad
    # user/pass, which is code 49
    else
      auth_error = ldap.inspect.include? "resultCode=>49"
    end

    if authed || Rails.env.test?
      log_in(params[:session][:user])
      return redirect_to "/new"
    elsif auth_error
      flash.now[:error] = "Bad email/password combination."
      return render "new"
    else
      flash.now[:error] = "Couldn’t connect to Active Directory.  "\
                          "Let IT know if this persists."
      return render "new"
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
end
