# coding: utf-8
# frozen_string_literal: true

require "resque"

class ApplicationController < ActionController::Base
  include ApplicationHelper
  include SessionsHelper

  protect_from_forgery with: :exception

  def new
    @title = "Mint ARKs"

    return redirect_to root_url unless logged_in?

    # The DataCite element "Resource Type" has two parts: "general
    # type" has a controlled vocabulary while "specific type" can be
    # anything.  So we need to handle it in two separate form
    # elements: a <select> drop-down for the general type, and a text
    # field for the specific type.  Here we merge them together.
    merge_resourcetypes!(params)

    @profile = params[:profile].to_sym if params[:profile]
    # All objects, whether extracted from a spreadsheet or entered in
    # the form, are collected in this hash.  See README.md for an
    # overview of the data model.
    @objects = {}
    @objects[:new] = {}
    @objects[:updates] = {}

    if params[:spreadsheet]
      book = ExcelHelper.open(params[:spreadsheet])
      records = ExcelHelper.scrape(book, profile: @profile)

      @objects[:new] = records[:new]
      @objects[:updates] = records[:updates]

      #
      # Setting more instance variables for the views
      #
      @status = if @objects && @objects[:updates]&.all?(&:empty?)
                  :new
                else
                  :updates
                end

      profile_columns = case @profile
                        when :erc
                          ERC_ELEMENTS.map { |e| "erc_#{e}" }
                        when :dc
                          DC_ELEMENTS.map { |e| "dc_#{e}" }
                        when :datacite
                          DATACITE_ELEMENTS.map { |e| "datacite_#{e}" }
                        end

      @extra_columns = @objects[@status][@profile][0].keys.reject do |k|
        ::ApplicationHelper.standard_header? k
      end

      @all_columns = profile_columns +
                     %w[target ark id] +
                     @extra_columns

    # We enter this branch if the user provided object metadata
    # through the Mint-a-single-ARK form on the main page
    else
      return unless params[:new] && !params[:new][:erc].empty?

      # The Mint-a-single-ARK form can only yield one object, but
      # @objects[:erc] is always an array, so our object will be the
      # first one
      to_mint = params[:new][:erc]

      previously_minted = ApplicationHelper.duplicates(
        to_mint,
        Item.where.not(target: "")
      )

      if previously_minted.empty?
        @objects[:new][:erc] = to_mint
      else
        @objects[:updates][:erc] = previously_minted
      end
    end

    return if @objects[:updates].all?(&:empty?)

    # If there are duplicates, render edit.html.erb; otherwise render
    # the default new.html.erb
    flash.now[:warning] = "The objects listed have already been minted. " \
                          "You may update their metadata below."
  end

  def update
    return redirect_to root_url unless logged_in?
    return redirect_to root_url unless (profile = params[:profile].to_sym)

    merge_resourcetypes!(params)

    @objects = {}
    @objects[:new] = {}

    # Update the modified object with Ezid and in our database
    @objects[:updates] = {}
    @objects[:updates][profile] = params[:updates][profile]

    unless params[:new].nil?
      @objects[:new][profile] = params[:new][profile].reject do |record|
        @objects[:updates][profile].include?(record)
      end
    end

    render "new"
  end

  def create
    @title = "Success"

    return redirect_to root_url unless logged_in?
    return redirect_to root_url unless (profile = params[:profile].to_sym)

    merge_resourcetypes!(params)

    @objects = {}
    @objects[:new] = params[:new] || {}
    @objects[:updates] = params[:updates] || {}

    new_duplicates = ApplicationHelper.duplicates(
      @objects[:new][profile],
      Item.where.not(target: "")
    )

    unless new_duplicates.empty?
      # Populate the duplicates bucket
      @objects[:updates][profile] ||= []
      @objects[:updates][profile] += new_duplicates
      @objects[:updates][profile].flatten!

      # Remove duplicates from the :new bucket
      @objects[:new][profile].reject! do |record|
        @objects[:updates][profile].any? do |dup|
          (!record["ark"].nil? && dup["ark"] == record["ark"]) ||
            (record["target"] != "" && dup["target"] == record["target"])
        end
      end

      flash.now[:warning] = "The objects listed have already been minted. " \
                            "You may update their metadata below."
      return render "edit" unless new_duplicates.empty?
    end

    resque_args = {
      "email" => params[:email],
      "profile" => params[:profile],
      "user" => params[:user],
    }
    if params[:updates]
      resque_args["updates"] = params[:updates][params[:profile]]
    end

    resque_args["new"] = params[:new][params[:profile]] if params[:new]

    Resque.enqueue(EzidHelper::Minter, resque_args)
  end

  def archive
    return redirect_to root_url unless logged_in?
    @title = "Records minted by #{current_user}"

    @objects = {}
    @objects[:archive] = {}

    PROFILE_SYMBOLS.each do |profile|
      @objects[:archive][profile] = {}
    end

    # See http://www.postgresqltutorial.com/postgresql-array/ for
    # array syntax
    Item.where("? = any(created_by)", current_user)
      .order(:job_id)
      .reverse_order
      .each do |record|

      record = record.as_json
      profile = record["profile"].to_sym

      # Non-standard headers (such as Aleph system number) are
      # collected as a JSON object in the :nonstandard_headers field
      # of PostgreSQL; here we unpack that object and return them to
      # first-class elements of the record.
      record["nonstandard_headers"].each do |k, v|
        record[k.to_sym] = v
      end

      job_key = record["job_id"].to_sym
      @objects[:archive][profile][job_key] ||= []
      @objects[:archive][profile][job_key] << record
    end
  end

  def download
    return redirect_to root_url unless logged_in?
    return redirect_to new_path unless (profile = params[:profile].to_sym)

    records = if params[:job_id]
                fetch_records(
                  profile: profile,
                  job_id: params[:job_id]
                )
              else
                fetch_records(
                  profile: profile,
                  user: params[:user]
                )
              end

    columns = case profile
              when :datacite
                DATACITE_ELEMENTS.map { |e| "datacite.#{e}" }
              when :dc
                DC_ELEMENTS.map { |e| "dc.#{e}" }
              when :erc
                ERC_ELEMENTS.map { |e| "erc.#{e}" }
              end

    columns << "location"
    columns << "ARK"
    columns += records.first.keys.reject do |k|
      ::ApplicationHelper.standard_header? k
    end

    workbook = RubyXL::Workbook.new
    ExcelHelper.write_sheet(workbook, columns, records)

    begin
      temp = Rails.root.join("tmp", "#{params[:filename]}.xlsx")
      workbook.write(temp)

      # Sending the data in a File.open block ensures that it's all
      # sent before the tempfile is removed
      File.open(temp, "r") do |f|
        send_data(
          f.read,
          filename: File.basename(temp),
          type: "application/"\
                "vnd.openxmlformats-officedocument"\
                ".spreadsheetml.sheet"
        )
      end
    ensure
      FileUtils.rm_f temp
    end
  end
end
