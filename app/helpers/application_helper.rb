# coding: utf-8
# frozen_string_literal: true

# Application helper methods
module ApplicationHelper
  # Return the CANDIDATES that match (ARK or target/location) any in AGAINST.
  #
  # @param [Hash] candidate An Ezid-ready object hash
  # @param [ActiveRecord::Relation, Array<Hash>] against
  #
  # @return [Array<Hash>]
  def self.duplicates(candidates, against)
    matches = []
    return matches if candidates.nil?

    candidates.each_with_index do |candidate, i|
      next if candidate[:ark].blank?

      db_record = Item.find_by(ark: candidate[:ark])
      # If we have that ARK in our database, add the DB ID to the
      # object so the existing DB record gets updated, instead of
      # creating a new one
      matches[i] = if db_record
                     candidate.merge(id: db_record[:id])
                   else
                     candidate
                   end
    end

    against.each do |db_record|
      candidates.each_with_index do |candidate, i|
        # If a duplicate of this candidate was found above, while
        # checking against ARKs
        next unless matches[i].nil?

        next unless candidate[:target]
        next unless candidate[:target] == db_record[:target]

        matches[i] = candidate.merge(id: db_record[:id], ark: db_record[:ark])
      end
    end
    matches
  end

  # The DataCite element "Resource Type" has two parts: "general type"
  # has a controlled vocabulary while "specific type" can be anything.
  # So we need to handle it in two separate form elements: a <select>
  # drop-down for the general type, and a text field for the specific
  # type.  This method merges them together.
  #
  # @param [ActionController::Parameters] params
  def merge_resourcetypes!(params)
    return unless (params[:new] && params[:new][:datacite]) ||
                  (params[:updates] && params[:updates][:datacite])

    # puts "Before: #{params.inspect}"

    [:new, :updates].each do |status|
      next unless params[status]

      params[status][:datacite].each_with_index do |param, i|
        next unless param[:datacite_resourcetype]

        params[status][:datacite][i][:datacite_resourcetype] +=
          "/#{param[:datacite_resourcesubtype]}"

        params[status][:datacite][i].delete :datacite_resourcesubtype
      end
    end

    # puts "After: #{params.inspect}"
  end

  # "Standard headers" include the elements of each metadata profile,
  # as well as defined column headers like ARK, location/target,
  # creation metadata, and other information.  Non-standard headers
  # are allowed but need special handling.
  #
  # @param [Symbol, String] header
  def self.standard_header?(header)
    CONTROLLED_HEADERS.include?(header.to_s
                                 .sub(/^erc_/, "")
                                 .sub(/^datacite_/, "")
                                 .sub(/^dc_/, ""))
  end

  def self.ezid_header?(header)
    EZID_HEADERS.include?(header.to_s
                           .sub(/^erc_/, "")
                           .sub(/^datacite_/, "")
                           .sub(/^dc_/, ""))
  end

  # Safari's JavaScript performance is noticably worse than other
  # browsers when rendering the interactive tables, due to the large
  # number of event listeners being processed.  So we issue a warning
  # for users about using Safari for large batches.
  def safari?
    request.env["HTTP_USER_AGENT"] &&
      request.env["HTTP_USER_AGENT"][/(Safari)/] &&
      !request.env["HTTP_USER_AGENT"][/(Chrome)/]
  end

  # Retrieve records from the DB and format them in a hash suitable
  # for writing to a spreadsheet with RubyXL.
  #
  # @param [Hash] options
  #
  # @option options [Symbol] :profile
  # @option options [String] :job_id
  # @option options [String] :user The user who ran the original
  #   job(s), corresponding to the `created_by` column in the database
  #
  # @return [Hash]
  def fetch_records(options = {})
    profile = options.fetch(:profile)
    query = if (job_id = options.fetch(:job_id, nil))
              { job_id: job_id }
            else
              [
                "? = any(created_by) AND profile = ?",
                options.fetch(:user),
                profile,
              ]
            end

    Item.where(query).map do |record|
      columns = case profile
                when :datacite
                  {
                    datacite_title: record[:datacite_title],
                    datacite_creator: record[:datacite_creator],
                    datacite_resourcetype: record[:datacite_resourcetype],
                    datacite_publicationyear: record[:datacite_publicationyear],
                    datacite_publisher: record[:datacite_publisher],
                    target: record[:target],
                    ark: record[:ark],
                  }
                when :dc
                  {
                    dc_title: record[:dc_title],
                    dc_creator: record[:dc_creator],
                    dc_type: record[:dc_type],
                    dc_date: record[:dc_date],
                    dc_publisher: record[:dc_publisher],
                    target: record[:target],
                    ark: record[:ark],
                  }
                when :erc
                  {
                    erc_what: record[:erc_what],
                    erc_who: record[:erc_who],
                    erc_when: record[:erc_when],
                    target: record[:target],
                    ark: record[:ark],
                  }
                end
      record["nonstandard_headers"].each do |k, v|
        columns[k.to_sym] = v
      end
      columns
    end
  end
end
