# frozen_string_literal: true

Rails.application.routes.draw do
  root "sessions#new"

  get "archive" => "application#archive"
  get "create" => "application#create"
  get "edit" => "application#edit"
  get "logout" => "sessions#destroy"
  get "new" => "application#new"
  post "create" => "application#create"
  post "download" => "application#download"
  post "login" => "sessions#create"
  post "new" => "application#new"
  post "update" => "application#update"
end
