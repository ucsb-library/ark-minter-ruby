# frozen_string_literal: true

module SessionsHelper
  def log_in(user)
    session[:user] = user.sub(/@.*/, "")
  end

  def current_user
    @current_user ||= session[:user]
  end

  def logged_in?
    current_user
  end

  def log_out
    session.delete(:user)
    @current_user = nil
  end
end
