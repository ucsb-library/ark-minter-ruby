# frozen_string_literal: true

require "test_helper"
require "ezid-client"

class EzidHelperTest < ActiveSupport::TestCase
  Item.all.select { |i| i["profile"] == "erc" }.each do |i|
    i.update_attributes(
      ark: Ezid::Identifier.mint(
        erc_what: i["erc_what"],
        erc_when: i["erc_when"],
        erc_who: i["erc_who"],
        target: i["target"]
      )
    )
  end

  test "update existing ARK" do
    record = Item.where(target: "http://alexandria.ucsb.edu/3").first.as_json
    record["erc_when"] = "1900"
    record["erc_what"] = "test"

    assert EzidHelper.update_arks("profile" => :erc,
                                  "updates" => [record],
                                  "user" => "test child")

    new_record = Ezid::Identifier.find(record["ark"])

    assert_equal "1900", new_record[:erc_when]
    assert_equal "test", new_record[:erc_what]
  end

  test "create a new ARK" do
    record = {
      "profile" => "erc",
      "erc_who" => "me",
      "erc_what" => "person",
      "erc_when" => "a while ago",
      "target" => "http://library.ucsb.edu",
    }
    assert EzidHelper.mint("new" => [record],
                           "profile" => :erc,
                           "user" => "smol")
    assert_equal 1, Item.where(erc_who: "me").count
  end
end
