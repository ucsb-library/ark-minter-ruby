# Summary: EZID Minter

This is a Rails web application for minting batches of ARKs.

Currently it

1. Accepts a spreadsheet (`.xls` or `.xlsx`) with object metadata
    described using the DataCite, DublinCore, or ERC
    [metadata profiles](http://ezid.cdlib.org/doc/apidoc.html#metadata-profiles);

2. Flags metadata that matches already-minted objects, and prompts the
   user to edit the existing record;

3. Updates and/or mints ARKs;

4. Allows the user to download an updated spreadsheet with the ARKs
   added;

5. Archives each user’s minting jobs so they can re-download them.

## Local development

1. Install dependencies: Ruby, PostgreSQL, Redis; start DBs
2. Create PostgreSQL database `ezid_dev`
3. `bundle install`
4. Start the development server with `make server`.
5. Start Resque with `make worker`.

- Run tests with `make test`.
- Generate [YARD docs](http://yardoc.org) with `make doc`.

### TODOs

- Allow authentication with systems other than Active Directory
- Allow batch edits within the web editor
- Allow comparison of local metadata for a record with CDL’s metadata
- Allow minting DOIs as well as ARKs
- Use the
  [Secret Server API](http://support.thycotic.com/kb/a186/java-client-console-api-for-accessing-secret-values-programatically.aspx?KBSearchID=105437)
  to fetch Ezid and PostgreSQL credentials for Passenger.

## Vagrant

- Provision the VM with `bin/provision vagrant`

- Deploy with Capistrano: `make vagrant`

## Production

- Provision the production server with `bin/provision production`

- Deploy with Capistrano: `SERVER=<hostname> REPO=https://<user>:<password>@github.library.ucsb.edu/ADRL/alexandria.git make prod`

# Overview: Authentication

Users log in using Active Directory.  Rails connects to
`dc.library.ucsb.edu` using the
[`net-ldap`](https://github.com/ruby-ldap/ruby-net-ldap) gem, and
checks if the authenticating user is in the following groups:

- `OU=EzID`
- `OU=Global Security Groups`
- `CN=EzID-Editors`

The authentication logic is in {SessionsController#create}; in future
it should be refactored to read the host, groups, etc. from a
configuration file.

# Overview: Views and Controllers

Most of the action happens in the {ApplicationController} and views.
The {SessionsController} is only used for logging in.

## Sessions

The site root is {SessionController#new}.  Users authenticate using
their Active Directory credentials.  If they are authorized to log in,
a session is created and they are redirected to
{ApplicationController#new}.

## Application

The data model is roughly this:

he `params` hash is parsed, and the `@objects` instance variable is
populated with records that the user wants ARKs minted for.  The
`@objects` variable is a hash with two keys: `:new` and
`:updates`. The values of the hashes are themselves also hashes, each
with three keys, corresponding to the three metadata profiles:
`:datacite`, `:dc`, and `:erc`.  The values of these hashes are
arrays, containing the records uploaded by the user.

```ruby
# A simple example of how @objects is structured
@objects = {
  :new => {
    :erc => [
      {:erc_what => "A cat", :erc_who => "Bastet", :erc_when => "A long time ago"},
      {:erc_what => "Nothing", :erc_who => "No one", :erc_when => "Never"},
    ]
    :dc => [
      {:dc_title => "Star War", :dc_creator => "George Lukacs"},
    ],
    :datacite => [],
  }
  :updates => {
    :erc => {
      {:erc_what => "An Everlasting Piece", :erc_who => "Billy Connolly",
       :erc_when => "2000", :ark => "ark:/99999/fk4f76kg6t"}
    }
    :dc => {},
    :datacite => {},
  },
}
```

The `:new` bucket is where all records begin; but by the end of a
session it should only contain records that definitely need ARKs
minted for them.

Objects that are flagged as duplicates are stored in
`@objects[:updates][:datacite]`, `@objects[:updates][:dc]`, and
`@objects[:updates][:erc]`.  Record are marked as duplicates if
there’s a record in the database with the same target/location URI or
the same ARK.

The application will not mint a new ARK for these records, but the
user is given the opportunity to update the metadata of the existing
record, and the updated record is saved to the database.

Views use `@objects` to pre-fill forms with values.  If the user is
prompted to edit a subset of the objects they uploaded, the rest are
preserved using hidden `<input>` elements.

The user submits the form, generating a new `params` hash.  `@objects`
is rebuilt using the new parameters, and the next view is rendered.
When all existing records have been updated and there are no more
duplicates, the records in `@objects[:new]` are minted.

### {ApplicationController#new}

The user selects a spreadsheet and a metadata profile, and
{ApplicationHelper::SRead.scrape} converts it to a hash.

If {ApplicationHelper.similar_objects} flags duplicates (existing
records with the same target/location URI or ARK), the user is sent to
{ApplicationController#update} and given the option of updating the
existing record instead of creating a duplicate.

After duplicates have been taken care of the user is given the
opportunity to edit the new records or add more to their spreadsheet
before sending the records to {ApplicationController#create} for
minting.

### {ApplicationController#update}

Whenever the application mints a new ARK for an object (see
{ApplicationController#create} below), it stores the object metadata
in the PostgreSQL database.  Every successive object provided by users
is checked against this database to ensure that we don’t mint a second
ARK for the same object.

Two different objects may have the same title (different versions of
the same book, two photographs by different artists, etc.), so all we
check is the target/location URI and the ARK.

If any objects are flagged as too similar during execution of the
{ApplicationController#new} action, they are discarded.  The
_original_ object in the database that they were too similar to is
placed in `@objects[:dups]`, and control is passed to the
{ApplicationController#update} action, which allows the user to modify
the metadata of that existing record (to correct the typo, or
whatever).  The
[`ezid-client`](https://github.com/duke-libraries/ezid-client) gem is
used to send the update to EZID.  The database entry for the object is
also updated, and control is passed back to
{ApplicationController#new}.

### {ApplicationController#create}

When the user has dealt with any duplicate objects (see
{ApplicationController#update} above), they hit “Finish” and control
is passed to {ApplicationController#create}.  The following steps are
taken:

1. Any original objects—ones that aren’t updated duplicates—are given
   permanent ARK identifiers.

2. Any existing objects have their metadata updated.

3. A new database entry is created for each original object, and
   database entries of existing records are updated.

These steps are performed in the background by Resque; an email is
sent to the user when the job completes.

### {ApplicationController#download}

Once objects have been updated and/or created, the user is prompted to
download a spreadsheet containing the metadata for their objects.  The
[`RubyXL`](https://github.com/weshatheleopard/rubyXL) gem is used by
{ApplicationHelper::SWrite.sheet} to create a new spreadsheet with
worksheets for DataCite, DublinCore, and/or ERC.

# Overview: components

## Database: PSQL

Minted ARKs along with their metadata are stored in PostgreSQL.  This
allows us to check for duplicates and lets users re-download
previously minted batches.

**Do not** migrate to a different database!  The application uses two
[data types specific to PSQL](http://stackoverflow.com/a/17918118):
JSON and arrays.

[JSON](http://www.postgresql.org/docs/current/static/datatype-json.html)
is used for the `nonstandard_headers` field, since we’d otherwise need
an arbitrary number of columns (because users can use an arbitrary
number of non-standard headers.

[Arrays](http://www.postgresql.org/docs/current/static/arrays.html)
are used for the `created_by` field, since a user can update records
originally created by a different user, and the record should then
appear in both of their archives.

## Job Queue: Resque

Because minting large batches can take minutes or hours, we don’t make
the user wait with the browser window open for the job to finish.
Instead it’s send to [Resque](https://github.com/resque/resque), which
places jobs in a queue and processes them one by one (it can run
multiple jobs in parallel, but according to EZID, minting many ARKs
simultaneously requires monitoring delays and adjusting timeouts).

When the job finishes, the user is sent an email notification (a happy
email if all the records were minted, a less-happy one if there were
one or more errors).

### Resque in production

Since Resque processes records with a method defined in the Ruby code,
the worker needs to be restarted when new code is deployed.
Unfortunately I haven’t been able to stop and start the background
Resque job using Capistrano while logging correctly (with or without
the
[capistrano-resque](https://github.com/sshingler/capistrano-resque)
plugin).

The current workaround is to install `resque_stop.sh` and
`resque_start.sh` on the production machine (see
`provisioning/roles/application/templates`).  `resque_stop.sh` reads
the pidfile that Resque writes when it starts, and kills the
corresponding job; `resque_start.sh` starts a Resque worker.

The Makefile target for `prod` runs `resque_stop.sh` on the remote
server, then runs `cap deploy`, then `resque_start.sh` on the remote
server.
