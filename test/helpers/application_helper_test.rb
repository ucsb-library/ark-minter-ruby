# frozen_string_literal: true

require "test_helper"

class ApplicationHelperTest < ActiveSupport::TestCase
  include ApplicationHelper

  test "fetch records from adunn" do
    records = fetch_records(
      profile: :datacite,
      user: "adunn"
    )

    assert_equal 3, records.count
  end

  test "fetch a records from a job" do
    records = fetch_records(
      profile: :erc,
      job_id: "adunn_1458582789"
    )
    assert_equal 1, records.count
    assert_equal "2167924", records.first[:"System number"]
  end
end
